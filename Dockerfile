# pull official base image
FROM python:3.9-buster

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# set work directory
WORKDIR /usr/src/app

# create the appropriate directories
RUN mkdir -p /static
RUN mkdir -p /media

RUN useradd -ms /bin/bash app

RUN chown app:app /static /media

ENV HOME=/home/app
ENV APP_HOME=/home/app/web

RUN mkdir $APP_HOME


RUN apt-get update && \
  apt-get install -y \
    netcat gettext

ENTRYPOINT ["/home/app/web/entrypoint.sh"]

# install dependencies
COPY ./requirements.txt /usr/src/app/requirements.txt

RUN pip install --upgrade pip
RUN pip install -r requirements.txt

WORKDIR $APP_HOME

# copy project
COPY --chown=app:app . $APP_HOME

# change to the app user
USER app

RUN python manage.py compilemessages

