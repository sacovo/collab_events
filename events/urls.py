from django.urls.conf import path

from events.views import CategoryListView, EventCreateView, EventDetailView, EventInternalDetailView, EventListView, EventUpdateView, LandingView, MyEventList, ReviewEventList, category_detail, ical_view

app_name = "events"

urlpatterns = [
    path("category/", CategoryListView.as_view(), name="category_list"),
    path("category/<int:pk>/", category_detail, name="category_detail"),
    path("<int:pk>/", EventDetailView.as_view(), name="event_detail"),
    path("<int:pk>/internal/",
         EventInternalDetailView.as_view(),
         name="event_internal"),
    path("<int:pk>/update/", EventUpdateView.as_view(), name="event_update"),
    path("<int:pk>/ical/", ical_view, name="event_ical"),
    path("create/", EventCreateView.as_view(), name="event_create"),
    path("events/", EventListView.as_view(), name="event_list"),
    path("my/", MyEventList.as_view(), name="my_events"),
    path("review/", ReviewEventList.as_view(), name="review_events"),
    path("", LandingView.as_view(), name="landing"),
]
