from django.db.models.query import QuerySet
from django.utils.translation import get_language
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, viewsets
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from events.models import Category, Event
from events.serializers import CategorySerializer, EventSerializer


class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    search_fields = ['title', 'description', 'category__name', 'city']
    filterset_fields = {
        'title': ('exact', 'icontains'),
        'description': ('icontains', ),
        'city': ('exact', 'icontains'),
        'date': ('lt', 'gt'),
        'is_online': ('exact', ),
        'is_offline': ('exact', ),
        'category': ('exact', ),
    }

    def get_queryset(self) -> QuerySet[Event]:
        if self.request.user.is_authenticated:
            return Event.objects.all().exclude(state=Event.HIDDEN)
        return Event.objects.filter(state=Event.PUBLIC)


class CategoryViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    filterset_fields = ['name', 'language_code']

    def get_queryset(self) -> QuerySet[Category]:
        return Category.objects.filter(language_code=get_language())
