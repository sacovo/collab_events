from typing import Dict
from events.models import Event


def review_count(request) -> Dict:
    if request.user.is_authenticated:
        return {
            'review_count': Event.objects.filter(state=Event.REVIEW).count()
        }
    return {}
