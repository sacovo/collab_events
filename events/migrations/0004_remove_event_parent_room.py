# Generated by Django 3.2 on 2021-04-18 20:29

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0003_auto_20210418_2224'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='parent_room',
        ),
    ]
