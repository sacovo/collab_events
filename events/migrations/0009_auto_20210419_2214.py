# Generated by Django 3.2 on 2021-04-19 20:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('events', '0008_alter_event_options'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='image',
            field=models.ImageField(blank=True, upload_to='images/%Y/%m/%d/', verbose_name='Bild'),
        ),
        migrations.AlterField(
            model_name='event',
            name='state',
            field=models.CharField(choices=[('draft', 'Entwurf'), ('review', 'Review'), ('public', 'Öffentlich'), ('hidden', 'Versteckt')], default='draft', max_length=12),
        ),
    ]
