"""
Model definitions for the events app
"""
from urllib.parse import quote
import bleach
from uuid import uuid4

from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.db.models.query_utils import Q
from django.urls.base import reverse
from django.utils.translation import gettext_lazy as _
from djrichtextfield.models import RichTextField

from events.rocket import create_discussion


class Category(models.Model):
    """
    Category of events, can include: workshop, flyer
    """
    name = models.CharField(max_length=255)
    slug = models.SlugField(unique=True)
    language_code = models.CharField(max_length=12, choices=settings.LANGUAGES)
    icon = models.FileField(blank=True)

    def __str__(self):
        return self.name


class Channel(models.Model):
    name = models.CharField(max_length=255)
    rid = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class Event(models.Model):
    """
    A single event
    """
    DRAFT = 'draft'
    REVIEW = 'review'
    PUBLIC = 'public'
    HIDDEN = 'hidden'

    STATES = (
        (DRAFT, _('Entwurf')),
        (REVIEW, _('Review')),
        (PUBLIC, _('Öffentlich')),
        (HIDDEN, _('Versteckt')),
    )

    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    title = models.CharField(
        max_length=255,
        verbose_name=_("Titel"),
        help_text=_("Titel deiner Veranstaltung"),
    )
    date = models.DateTimeField(
        verbose_name=_("Datum"),
        help_text=_("Datum und Zeit"),
    )
    duration = models.DurationField(verbose_name=_("Dauer"))
    language_code = models.CharField(max_length=12, choices=settings.LANGUAGES)
    description = RichTextField(
        sanitizer=lambda text: bleach.clean(text,
                                            tags=bleach.sanitizer.ALLOWED_TAGS
                                            + ['p', 'h2', 'h3', 'h4']),
        help_text=_("Um was geht es bei deiner Veranstaltung?"),
        verbose_name=_("Beschreibung"),
    )

    owner = models.ForeignKey(get_user_model(), models.CASCADE)
    state = models.CharField(
        choices=STATES,
        default=DRAFT,
        max_length=12,
        help_text=
        _("Sobald dein Event bereit ist, kannst du ihn auf Review stellen, damit er freigeschaltet wird."
          ))
    category = models.ForeignKey(Category,
                                 models.CASCADE,
                                 verbose_name=_("Kategorie"))
    internal_id = models.UUIDField(default=uuid4)
    image = models.ImageField(
        verbose_name=_("Bild"),
        blank=True,
        upload_to="images/%Y/%m/%d/",
    )

    is_online = models.BooleanField(
        verbose_name=_("Kann man bei deinem Event online teilnehmen?"), )
    is_offline = models.BooleanField(
        verbose_name=_("Kann man beim Event vor Ort teilnehmen?"), )

    link = models.URLField(
        blank=True,
        verbose_name=_("Link"),
        help_text=_(
            "Link zu deiner Veranstaltung, oder zu einem Anmeldeformular"),
    )

    street = models.CharField(
        max_length=200,
        blank=True,
        verbose_name=_("Strasse"),
    )
    city = models.CharField(max_length=200, blank=True, verbose_name=_("Ort"))
    zip_code = models.CharField(
        max_length=10,
        blank=True,
        verbose_name=_("PLZ"),
    )

    room = models.ForeignKey(
        Channel,
        models.SET_NULL,
        blank=True,
        null=True,
        verbose_name=_("Rocketchat-Raum"),
        help_text=
        _("Wähle einen Raum von Rocket-Chat aus, um in diesem automatisch eine Diskussion "
          "zu starten um den Event zu planen. Du kannst es auch leer lassen."),
    )
    discussion_id = models.CharField(max_length=200, blank=True)

    def has_edit_permission(self, user):
        """Checks if the given user may edit the event."""
        if user == self.owner:
            return True
        return EditAccess.objects.filter(
            user=user,
            event=self,
            state=EditAccess.GRANTED,
        ).exists()

    def map_url(self):
        query = f"{self.street}, {self.zip_code} {self.city}"
        return f"https://www.google.com/maps/search/?api=1&query={quote(query)}"

    class Meta:
        constraints = [
            models.CheckConstraint(
                check=Q(is_offline=False)
                | (~Q(street__exact='') & ~Q(city__exact='')
                   & ~Q(zip_code__exact='')),
                name="offline_event_has_address",
            ),
        ]
        ordering = ['date']
        permissions = [
            ("publish_event", _("Kann Events publizieren.")),
        ]

    def __str__(self):
        return f"Event: {self.title}"

    def get_absolute_url(self):
        return reverse("events:event_detail", args=(self.pk, ))

    def chat_link(self):
        return f"https://{settings.ROCKET_DOMAIN}/channel/{self.discussion_id}"

    def save(self, *args, **kwargs):
        if self.room and not self.discussion_id:
            self.discussion_id = create_discussion(
                self.room.rid,
                self.title,
            )
        self.language_code = self.category.language_code
        return super().save(*args, **kwargs)


class EditAccess(models.Model):
    """
    Gain access to edit an event through this.
    """
    PENDING = 0
    GRANTED = 1
    DENIED = 2

    STATES = (
        (_("pending"), PENDING),
        (_("granted"), GRANTED),
        (_("denied"), DENIED),
    )

    event = models.ForeignKey(
        Event,
        on_delete=models.CASCADE,
        related_name="editors",
    )

    user = models.ForeignKey(get_user_model(), models.CASCADE)

    created = models.DateTimeField(auto_now_add=True)
    state = models.IntegerField(choices=STATES)

    def __str__(self):
        return f"EditAccess: {self.user.username}@{self.event.slug}"
