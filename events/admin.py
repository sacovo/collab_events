from events.rocket import fetch_channels
from events.models import Category, Channel, EditAccess, Event
from django.contrib import admin

# Register your models here.


@admin.register(Channel)
class ChannelAdmin(admin.ModelAdmin):
    list_display = ['name', 'rid']
    actions = ['update_rooms']

    def update_rooms(self, request, queryset):
        for channel in fetch_channels():
            Channel.objects.update_or_create(
                rid=channel['_id'],
                defaults={
                    'name': channel['name'],
                },
            )


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    """
    Admin for events
    """
    list_display = ['title', 'state', 'owner', 'category']
    list_filter = ['state', 'category']

    actions = ['publish_selected', 'hide_selected', 'mark_as_draft']

    def publish_selected(self, request, queryset):
        queryset.update(state=Event.PUBLIC)

    def hide_selected(self, request, queryset):
        queryset.update(state=Event.HIDDEN)

    def mark_as_draft(self, request, queryset):
        queryset.update(state=Event.DRAFT)


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    """
    Admin for categories
    """
    pass


@admin.register(EditAccess)
class EditAccessAdmin(admin.ModelAdmin):
    """
    Admin for edit access
    """
    pass
