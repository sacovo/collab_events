from django.conf import settings
import requests


def get_auth_headers():
    return {
        'X-Auth-Token': settings.ROCKET_AUTH,
        'X-User-Id': settings.ROCKET_USER,
    }


def create_discussion(parent_room: str, title: str) -> str:
    response = requests.post(
        f"https://{settings.ROCKET_DOMAIN}/api/v1/rooms.createDiscussion",
        headers=get_auth_headers(),
        json={
            "prid": parent_room,
            "t_name": title,
        })
    if response.status_code == 200:
        return response.json()['discussion']['name']
    return "no-dicussion"


def send_message(rid, message):
    requests.post(
        f"https://{settings.ROCKET_DOMAIN}/api/v1/chat.sendMessage",
        headers=get_auth_headers(),
        json={
            'message': {
                "rid": rid,
                "msg": message,
            },
        },
    )


def fetch_channels():
    return requests.get(
        f"https://{settings.ROCKET_DOMAIN}/api/v1/channels.list",
        headers=get_auth_headers(),
    ).json()['channels']
