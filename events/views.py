from datetime import datetime
from typing import Any, Dict
from django.db.models.query import QuerySet
from django.forms.models import BaseModelForm
from django.http.response import HttpResponse
from django.shortcuts import redirect
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import get_language
from django.views.generic import DetailView
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView
from django_filters.views import FilterView
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from events.filters import EventFilter
from events.forms import EventForm
from events.models import Category, Event

from django.utils.translation import gettext as _
# Create your views here.


class EventDetailView(DetailView):
    model = Event

    def get_queryset(self) -> QuerySet:
        if self.request.user.is_authenticated:
            return super().get_queryset()
        return super().get_queryset().filter(state=Event.PUBLIC)


class EventInternalDetailView(LoginRequiredMixin, DetailView):
    model = Event
    template_name = 'events/event_internal.html'


class BaseEventView(FilterView):
    model = Event
    template_name = 'events/event_list.html'
    filterset_class = EventFilter
    paginate_by = 20

    def get_context_data(self, **kwargs: Any) -> Dict[str, Any]:
        result = super().get_context_data(**kwargs)
        if category_pk := self.request.GET.get('category'):
            result['title'] = _("Kategorie: ") + Category.objects.get(
                pk=category_pk).name
        else:
            result['title'] = self.title
        return result


class EventListView(BaseEventView):
    title = _("Alle Events")

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return Event.objects.filter(
                date__date__gte=timezone.now().date(),
                language_code=get_language(),
            )
        return Event.objects.filter(
            date__date__gte=timezone.now().date(),
            language_code=get_language(),
            state=Event.PUBLIC,
        )


class MyEventList(LoginRequiredMixin, BaseEventView):
    title = _("Meine Events")

    def get_queryset(self):
        return Event.objects.filter(owner=self.request.user, )


class ReviewEventList(PermissionRequiredMixin, BaseEventView):
    permission_required = "event.publish_event"
    title = _("Review Events")

    def get_queryset(self):
        return Event.objects.filter(state=Event.REVIEW)


class EventCreateView(LoginRequiredMixin, CreateView):
    model = Event
    form_class = EventForm
    template_name = 'events/event_create.html'

    def form_valid(self, form: BaseModelForm) -> HttpResponse:
        form.instance.owner = self.request.user
        form.instance.language_code = get_language()

        return super().form_valid(form)

    def get_form_kwargs(self) -> Dict[str, Any]:
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


class EventUpdateView(LoginRequiredMixin, UpdateView):
    model = Event
    form_class = EventForm
    template_name = 'events/event_update.html'

    def get_queryset(self) -> QuerySet:
        if self.request.user.has_perm('event.publish_event'):
            return super().get_queryset()

        return super().get_queryset().filter(owner=self.request.user)

    def get_form_kwargs(self) -> Dict[str, Any]:
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user})
        return kwargs


class CategoryListView(ListView):
    model = Category

    def get_queryset(self) -> QuerySet:
        return super().get_queryset().filter(language_code=get_language())


class LandingView(TemplateView):
    template_name = 'events/landing.html'


def category_detail(request, pk):
    return redirect(reverse('events:event_list') + f"?category={pk}")


def format_for_ical(d):
    return datetime.strftime(d, "%Y%m%dT%H%M%S")


def ical_view(request, pk):
    response = HttpResponse(
        content_type="text/calendar, text/x-vcalendar, application/hbs-vcs")
    response["Content-Disposition"] = 'attachment; filename="event.ics"'
    event = Event.objects.get(pk=pk)
    response.write(f"""BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//Collab-EVENT Tool//v0.1
BEGIN:VEVENT
UID:{event.pk}@{request.get_host()}
CREATED:{format_for_ical(event.created)}Z
DTSTAMP:{format_for_ical(event.updated)}Z
DTSTART:{format_for_ical(event.date)}Z
DTEND:{format_for_ical(event.date + event.duration)}Z
SUMMARY:{event.title}
DESCRIPTION:https://{request.get_host()}{reverse("events:event_detail", args=(event.pk,))}
END:VEVENT
END:VCALENDAR
""")
    return response
