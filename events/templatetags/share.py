from urllib.parse import quote, urlencode
from django import template

register = template.Library()


@register.simple_tag
def facebook(url):
    return "https://www.facebook.com/sharer/sharer.php?u=" + quote(url)


@register.simple_tag
def twitter(url):
    return "http://www.twitter.com/share?url=" + quote(url)


@register.simple_tag
def linkedin(url, title):
    return f"https://www.linkedin.com/shareArticle?title={quote(title)}&url={quote(url)}"


@register.simple_tag
def mail(url, title):
    return f"mailto:?subject={quote(title)}&body={quote(url)}"
