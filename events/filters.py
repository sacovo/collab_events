from functools import reduce
from django.db.models.query import QuerySet
from django.db.models.query_utils import Q
from events.models import Category, Event
from django.utils.translation import get_language, gettext as _
import django_filters


def categories_for_language(request):
    return Category.objects.filter(language_code=get_language())


class EventFilter(django_filters.FilterSet):
    search = django_filters.CharFilter(
        field_name=_("search"),
        method="filter_search_fields",
        label=_("Veranstaltungen durchsuchen"),
    )

    search_fields = ['title', 'description', 'category__name', 'city']

    date_gt = django_filters.DateFilter(
        field_name="date",
        lookup_expr="gt",
        label=_("Von"),
    )
    date_lt = django_filters.DateFilter(
        field_name="date",
        lookup_expr="lt",
        label=_("Bis"),
    )

    city_icontains = django_filters.CharFilter(
        field_name="city",
        lookup_expr="icontains",
        label=_("Gemeinde"),
    )

    category = django_filters.ModelChoiceFilter(
        field_name="category",
        lookup_expr="exact",
        queryset=categories_for_language,
        label=_("Kategorie"),
    )

    is_online = django_filters.BooleanFilter(
        field_name="is_online",
        lookup_expr="exact",
        label=_("Online"),
    )
    is_offline = django_filters.BooleanFilter(
        field_name="is_offline",
        lookup_expr="exact",
        label=_("Offline"),
    )

    class Meta:
        model = Event
        fields = []

    def get_search_filter_query(self, value) -> Q:
        return reduce(lambda acc, val: acc | val, [
            Q(**{field + "__icontains": value}) for field in self.search_fields
        ])

    def filter_search_fields(self, queryset, name, value) -> QuerySet[Event]:
        return queryset.filter(self.get_search_filter_query(value))

    @property
    def qs(self):
        parent = super().qs

        if self.request.user.is_authenticated:
            return parent

        return parent.filter(state=Event.PUBLIC)
