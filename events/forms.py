from typing import Any, Dict

from django.core.exceptions import ValidationError
from events.models import Event
from django.utils.translation import gettext as _
from django import forms


class EventForm(forms.ModelForm):
    class Meta:
        model = Event
        fields = [
            'title', 'description', 'date', 'duration', 'category', 'image',
            'is_online', 'is_offline', 'link', 'street', 'city', 'zip_code',
            'room', 'state'
        ]
        widgets = {
            'duration':
            forms.TextInput(attrs={
                'pattern': '[0-9]{2}:[0-9]{2}(:[0-9]{2})?',
                'placeholder': 'HH:MM'
            }),
            'date':
            forms.DateTimeInput(format="%Y-%m-%d %H:%M:%S")
        }

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super().__init__(*args, **kwargs)
        if (self.user is
                None) or (not self.user.has_perm('event.publish_event')):
            if self.instance.state == Event.PUBLIC:
                self.fields['state'].choices = (
                    (Event.DRAFT, _("Entwurf")),
                    (Event.REVIEW, _("Review")),
                    (Event.PUBLIC, _("Publiziert")),
                )
            else:
                self.fields['state'].choices = (
                    (Event.DRAFT, _("Entwurf")),
                    (Event.REVIEW, _("Review")),
                )

    def clean(self) -> Dict[str, Any]:
        if self.cleaned_data['is_offline'] and not (
                self.cleaned_data['street'] and self.cleaned_data['city'] and
                self.cleaned_data['city'] and self.cleaned_data['zip_code']):
            self.add_error(
                "street",
                ValidationError(
                    _("Ein offline Event braucht eine vollständige Adresse.")))
        return super().clean()
