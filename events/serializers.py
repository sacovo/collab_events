"""
Serializers for events
"""
from django.utils.translation import ugettext as _
from typing import Any
from rest_framework import serializers

from events.models import Category, Event


class CategorySerializer(serializers.ModelSerializer):
    """
    Serializer for categories
    """
    class Meta:
        model = Category
        fields = ['name', 'slug']


class EventSerializer(serializers.ModelSerializer):
    """
    Serializer for events
    """
    class Meta:
        model = Event
        fields = [
            'title',
            'date',
            'language_code',
            'description',
            'is_online',
            'is_offline',
            'state',
            'category',
            'street',
            'city',
            'zip_code',
            'link',
            'room',
            'discussion_id',
        ]
        read_only_fields = ['state', 'discussion_id']

    def validate(self, data):
        if data['is_offline'] and not (data['street'] and data['city']
                                       and data['zip_code']):
            raise serializers.ValidationError(
                _("address may not be empty if event is offline"))
        return data

    def create(self, validated_data: Any) -> Event:
        return Event.objects.create(owner=self.context['request'].user,
                                    **validated_data)

    def update(self, instance: Event, validated_data: Any) -> Event:
        if not instance.has_edit_permission(self.context['request'].user):
            raise serializers.PermissionDenied(
                _("you don't have the permission to edit this event!"))
        return super().update(instance, validated_data)
