from rest_framework import routers

from events.viewsets import CategoryViewSet, EventViewSet

router = routers.DefaultRouter()

router.register('event', EventViewSet)
router.register('category', CategoryViewSet)

urlpatterns = router.urls
